# corruption_fighter

Тестовое задание №2.

## Как запускать

1. Установить `python3` и `pip`.
2. Запустить `pip install -r requirements.txt`.
3. Запустить `./main.py --config config.json`. Результаты будут сохраненены в папке `output` в csv-файлах.
