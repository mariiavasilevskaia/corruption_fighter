#!/usr/bin/env python3

import standard_form_parser as sfp

import pandas as pd

import argparse
import json
import os
import shutil
from functools import reduce


def parse_declaration(declaration_type, ministry, year, path, use_spellchecker):
    if declaration_type == "standard_form":
        return sfp.parse_workbook(path, ministry, year, use_spellchecker)

    raise Exception(f"Type {declaration_type} is not supported")


def generate_tables(config, use_spellchecker=False):
    result = {
        "households": [],
        "office": [],
        "owned": [],
        "used": [],
        "vehicles": [],
        "income": [],
        "sources": [],
    }

    for entry in config:
        for year, declaration_path in entry["declarations"].items():
            tables = parse_declaration(
                entry["declaration_type"], entry["ministry"], year, declaration_path, use_spellchecker
            )
            for name, table in tables.items():
                result[name].append(table)

    for name in tables:
        if name == "households":
            result[name] = reduce(
                lambda left, right: pd.merge(left, right, how="outer"), result[name]
            )
            result[name].set_index("name", inplace=True)
            result[name].sort_index(inplace=True)
        else:
            result[name] = pd.concat(result[name])
            result[name].reset_index(drop=True, inplace=True)

    return result


def parse_args():
    parser = argparse.ArgumentParser(description="Fight corruption with code!")
    parser.add_argument(
        "--config", type=str, required=True, help="Path to input json configuration"
    )
    parser.add_argument(
        "--use_spellchecker", type=bool, default=False, help="Use spellchecker on fields (might be slow)"
    )

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()

    with open(args.config, "r") as fh:
        config = json.load(fh)

    tables = generate_tables(config, args.use_spellchecker)

    if os.path.exists("output"):
        shutil.rmtree("output")
    os.mkdir("output")
    for name, table in tables.items():
        table.to_csv(f"output/{name}.csv")
