import xls_utils as xu
import text_utils as tu

import pandas as pd
import numpy as np
from openpyxl import load_workbook

import re


def extract_title(sheet):
    title = ""
    for row in sheet.rows:
        for cell in row:
            if cell.value:
                title = cell.value
                break
        if title:
            break

    title = " ".join(title.splitlines())
    return title


def extract_year(sheet):
    title = extract_title(sheet)

    year_re = re.compile("(?:19|20)\d{2}")
    unique_matches = list(set(year_re.findall(title)))
    if len(unique_matches) != 1:
        return None

    return int(unique_matches[0])


def simple_cleanup(sheet):
    """
    Заменяем переносы строк внутри ячеек на пробелы
    Удаляем лишние пробельные символы
    Заменяем пустые ячейки на прочерки
    Заменяем запятые на точки
    """
    number_re = re.compile("^\d+(?:\.\d*)?$")

    def is_number(value):
        return number_re.search(value)

    for i in range(1, sheet.max_row + 1):
        for j in range(1, sheet.max_column):
            value = sheet[i][j].value
            if value:
                value = str(value)
                value = " ".join(value.splitlines()).strip()
                value = value.replace(",", ".")
                if is_number(value):
                    value = float(value)
                sheet[i][j].value = value
            elif value == "":
                sheet[i][j].value = "-"


def deduplicate_people_within_household(df):
    def deduplicate(x):
        x["name_suffix"] = x.groupby(["name"]).cumcount()
        return x

    df = df.groupby(["local_id"]).apply(deduplicate)
    df.loc[df.name.isna(), "name_suffix"] = 0
    df.loc[df["name_suffix"] > 0, "name"] += " " + (
        df.loc[df["name_suffix"] > 0, "name_suffix"] + 1
    ).astype(str)
    df.drop("name_suffix", axis=1, inplace=True)


def extract_family_role(df):
    first_letter_re = re.compile("^[А-ЯЁЙ]")

    def is_initials(text):
        return first_letter_re.search(text)

    df.insert(
        1,
        "family_role",
        [
            df["name"].iloc[i]
            if not is_initials(df["name"].iloc[i])
            else "декларирующий"
            for i in range(len(df.index))
        ],
    )

    df.loc[df["family_role"] != "декларирующий", "name"] = np.nan
    df["name"] = df["name"].fillna(method="ffill")


def prepare_table(ws, use_spellchecker):
    """
    Названия для столбцов
    Создание корретной пары (имя министра в семействе, роль в семье) для каждой записи
    Дедупликация одинаковых детей
    Замена все возможные типы пропусков на np.nan
    """
    df = pd.DataFrame(ws.values)
    df.dropna(axis=1, how="all", inplace=True)  # иногда встречаются пустые столбцы
    df.columns = [
        "local_id",
        "name",
        "office",
        "property_owned",
        "property_owned_type",
        "property_owned_sqm",
        "property_owned_country",
        "property_used",
        "property_used_sqm",
        "property_used_country",
        "vehicle",
        "income",
        "source",
    ]
    # Первые две строки содержат исходные названия колонок
    df.drop([0, 1], inplace=True)

    # Удаляем мусорную строчку с номерами столбцов 1 2 3 4 5...
    if pd.to_numeric(df.iloc[0], errors='coerce').notnull().all():
        df.drop([2], inplace=True)

    # Для начала первая колонка будет задавать семью. В дальнейшем для возможности установления
    # связи между людьми в декларациях за разные годы идентификатором семьи будет ФИО госслужащего
    df["local_id"] = df["local_id"].fillna(method="ffill")

    deduplicate_people_within_household(df)

    # Только теперь мы готовы заполнить пропуски в колонке имен, иначе разные дети склеились бы в одного
    df["name"] = df["name"].fillna(method="ffill")
    df.drop("local_id", axis=1, inplace=True)

    extract_family_role(df)
    df["name"] = df["name"].apply(tu.normalize_full_name)

    for column in [
        "family_role",
        "office",
        "property_owned",
        "property_owned_type",
        "property_owned_country",
        "property_used",
        "property_used_country",
        "vehicle",
        "source",
    ]:
        df[column] = df[column].apply(lambda x: tu.normalize_text_field(x, use_spellchecker))

    # Грязный фикс для супругов
    df["family_role"] = df["family_role"].apply(lambda x: x.replace('c', 'с'))

    # Унифицируем пропуски
    df.replace('-', None, inplace=True)
    df.fillna(value=np.nan, inplace=True)

    df.reset_index(drop=True, inplace=True)
    return df


def create_df(main_df, columns, year=None, ministry=None):
    df = main_df[columns].drop_duplicates().dropna().reset_index(drop=True)
    if ministry is not None:
        df["ministry"] = ministry
    if year is not None:
        df["year"] = year

    return df


def parse_workbook(filename, ministry, year, use_spellchecker=False):
    year = int(year)

    wb = load_workbook(filename=filename)
    ws = wb.active

    xu.unmerge_cells(ws)
    simple_cleanup(ws)

    parsed_year = extract_year(ws)
    if parsed_year is not None:
        assert(parsed_year == year)
        ws.delete_rows(0, 1)

    df = prepare_table(ws, use_spellchecker)

    return {
        "households": create_df(df, ["name", "family_role"]),
        "office": create_df(df, ["name", "office"], year, ministry),
        "owned": create_df(
            df,
            [
                "name",
                "family_role",
                "property_owned",
                "property_owned_type",
                "property_owned_sqm",
                "property_owned_country",
            ],
            year,
        ),
        "used": create_df(
            df,
            [
                "name",
                "family_role",
                "property_used",
                "property_used_sqm",
                "property_used_country",
            ],
            year,
        ),
        "vehicles": create_df(df, ["name", "family_role", "vehicle"], year),
        "income": create_df(df, ["name", "family_role", "income"], year),
        "sources": create_df(df, ["name", "family_role", "source"], year),
    }
