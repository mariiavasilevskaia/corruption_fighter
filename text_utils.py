#!/usr/bin/env python3

from pyaspeller import Word
import re


def normalize_full_name(text):
    text = text.strip()
    capitalized = u"[А-ЯЙЁ][а-яйё]+"
    surname = capitalized + "(?:\-" + capitalized + "\s?\-?\s?)*"
    name = capitalized
    full_name = "^(" + surname + ")\s+" + "(" + name + ")\s(" + name + ")$"
    pattern = re.compile(full_name)
    match = pattern.search(text)
    if match:
        name_init = match.group(2)[0]
        patronym_init = match.group(3)[0]
        surname_init = match.group(1)
        return surname_init + " " + name_init + "." + patronym_init + "."
    else:
        return text


def correct_spelling(text):
    """
    Исправляем только очевидные опечатки пословно
    """
    result = []
    for word in text.split():
        check = Word(word)
        if check.correct or not check.spellsafe:
            result.append(word)
        else:
            result.append(check.spellsafe)
    return " ".join(result)


def normalize_text_field(text, use_spellchecker):
    if text is None:
        return None
    normalized_text = text.lower().strip()
    if use_spellchecker:
        return correct_spelling(normalized_text)
    return normalized_text


if __name__ == "__main__":
    assert normalize_full_name("Пугачев Павел Сергеевич") == "Пугачев П.С."
    assert normalize_full_name("Мусин-Пушкин Чувак Валерьевич") == "Мусин-Пушкин Ч.В."
    print("All tests passed")
