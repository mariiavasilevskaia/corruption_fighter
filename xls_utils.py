from openpyxl import load_workbook

import re

# Функции для работы с openpyxl


def unmerge_cells(sheet):
    merged_cells_ranges = sheet.merged_cells.ranges
    for cells_range in sorted(merged_cells_ranges):
        sheet.unmerge_cells(str(cells_range))


def copy_columns(sheet, end_row, cols):
    selected = []
    for i in range(0, end_row):
        selected_row = []
        for j in cols:
            selected_row.append(sheet.cell(row=i + 1, column=j + 1).value)
        selected.append(selected_row)
    return selected


def paste_columns(empty_sheet, buffer):
    for i in range(len(buffer)):
        for j in range(len(buffer[0])):
            empty_sheet.cell(row=i + 1, column=j + 1).value = buffer[i][j]


def print_row(row):
    for i in range(len(row)):
        print([row[i].value])


def replace_row(sheet, row, buffer):
    for i in range(len(buffer)):
        sheet.cell(row=row + 1, column=i + 1).value = buffer[i]
